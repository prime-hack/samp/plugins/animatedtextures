#include "Config.h"
#include "loader/loader.h"
#include <fstream>
#include <filesystem>
#include <unordered_map>

Config::Config( std::string_view fname ) : fname( fname ) {
	if ( !isConfigExists() ) return;
	std::ifstream cfg( this->fname );
	if ( !cfg.is_open() ) {
		MessageBox( "Can't open config", fname );
		return;
	}

	std::unordered_map<std::string, std::string> store;
	std::string									 line;
	while ( std::getline( cfg, line ) ) {
		auto pos_assign = line.find( '=' );
		if ( pos_assign == std::string::npos || pos_assign == 0 || pos_assign == line.length() - 1 ) continue;
		auto key   = line.substr( 0, pos_assign );
		auto value = line.substr( pos_assign + 1 );
		store[key] = value;
	}
	cfg.close();

	try {
		background.set( std::stoul( store["background"] ) );
		overrideSettings.set( std::stoul( store["override"] ) );
	} catch ( std::exception &e ) {
		MessageBox( e.what(), this->fname );
	}

	if ( !overrideSettings.isInitialized() || !overrideSettings.get() ) return;

	for ( auto &&[key, strValue] : store ) {
		try {
			auto value = std::stoul( strValue );
			if ( key == "number_color_palette" )
				paletteNum.set( value );
			else if ( key == "alpha" )
				alpha.set( value );
		} catch ( std::exception &e ) {
			MessageBox( e.what(), this->fname );
		}
	}
}

Config::~Config() {
	if ( !isConfigExists() ) return;
	std::ofstream cfg( fname );
	if ( !cfg.is_open() ) {
		MessageBox( "Can't create config", fname );
		return;
	}

	if ( background.isInitialized() )
		cfg << "background=" << std::dec << (int)background.get() << std::endl;
	else if ( overrideSettings.isInitialized() ) {
		cfg << "override=" << std::dec << (int)overrideSettings.get() << std::endl;
		if ( overrideSettings.get() ) {
			if ( paletteNum.isInitialized() )
				cfg << "number_color_palette=" << std::dec << (int)paletteNum.get() << std::endl;
			else if ( alpha.isInitialized() )
				cfg << "alpha=" << std::dec << (int)alpha.get() << std::endl;
		}
	}
	cfg.close();
}

bool Config::isConfigExists() const {
	return std::filesystem::exists( fname );
}
