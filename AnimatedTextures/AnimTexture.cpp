#include "AnimTexture.h"
#include <CGame/Types.h>
#include "loader/loader.h"
#include <GifPlayer.h>

AnimTexture::AnimTexture( std::string_view name, bool loadNow )
	: cfg( std::string( name ) + ".cfg" ), gifPath( std::string( name ) + ".gif" ) {
	if ( loadNow ) setLoadState( eLoadState::loadInRam );

	if ( !cfg.isConfigExists() ) return;

	if ( !cfg.overrideSettings.get() ) return;

	if ( cfg.paletteNum.isInitialized() ) palette = new RwPalette();
}

RwRaster *AnimTexture::animRaster( RwRaster *originalRaster ) noexcept {
	if ( !originalRaster ) return originalRaster; // do not set animation texture for nullptr rasters

	if ( !isEventsInitialized ) {
		isEventsInitialized = true;
		g_class.draw->onPreReset += [this] {
			if ( player && loadState == eLoadState::loadFull ) player->Invalidate();
		};
		g_class.draw->onPostReset += [this] {
			if ( player && loadState == eLoadState::loadFull ) player->Initialize( g_class.draw->d3d9_device() );
		};
	}

	if ( loadState != eLoadState::loadFull ) setLoadState( eLoadState::loadFull );
	lastUsage = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );

	auto texture = player->ProcessPlay();
	if ( player->IsLoopEnded() ) player->ResetLoop();

	if ( originalRaster == lastMimic ) {
		raster.getRasterExt()->texture = texture;
		return raster.getRaster();
	} else {
		raster.mimic( originalRaster );
		lastMimic = originalRaster;

		// Store overridable data to config
		if ( cfg.isConfigExists() && !cfg.overrideSettings.get() ) {
			if ( lastMimic->rwD3D9RasterExt()->palette )
				cfg.paletteNum.set( ( (RwPalette *)lastMimic->rwD3D9RasterExt()->palette )->paletteNumber );
			cfg.alpha.set( lastMimic->rwD3D9RasterExt()->alpha );
		}
	}

	raster.getRasterExt()->texture = texture;
	if ( !cfg.overrideSettings.get() ) return raster.getRaster();
	if ( palette ) {
		palette->paletteNumber		   = cfg.paletteNum.get();
		raster.getRasterExt()->palette = (DWORD)palette;
	}
	if ( cfg.alpha.isInitialized() ) raster.getRasterExt()->alpha = cfg.alpha.get();
	return raster.getRaster();
}

AnimTexture::eLoadState AnimTexture::getLoadState() const {
	return loadState;
}

void AnimTexture::setLoadState( eLoadState state ) {
	if ( state == loadState ) return;

	switch ( state ) {
		case eLoadState::unload:
			delete player;
			break;
		case eLoadState::loadInRam:
			if ( loadState == eLoadState::unload ) {
				player = new GifPlayer( gifPath.c_str() );
				if ( cfg.background.isInitialized() && cfg.background.get() )
					player->ToggleBgNoUpdate( true );
				else
					player->ToggleBgNoUpdate( false );
			} else
				player->Invalidate();
			break;
		case eLoadState::loadFull:
			if ( loadState == eLoadState::unload ) setLoadState( eLoadState::loadInRam );
			player->Initialize( g_class.draw->d3d9_device() );
			break;
	}

	loadState = state;
}

std::chrono::milliseconds AnimTexture::getLastUsage() const {
	return lastUsage;
}
