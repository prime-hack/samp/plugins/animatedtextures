#include "main.h"
#include <CGame/Types.h>
#include <filesystem>
#include <thread>
#include "AnimTexture.h"

namespace fs = std::filesystem;
using namespace std::chrono_literals;

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	did = g_class.draw->onDraw += std::tuple{ this, &AsiPlugin::draw };

	if ( !fs::is_directory( PROJECT_NAME ) ) {
		if ( fs::exists( PROJECT_NAME ) ) fs::remove( PROJECT_NAME );
		fs::create_directory( PROJECT_NAME );
	}

	std::thread( [this] {
//		auto t0 = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );
		for ( auto preloadCounter = 0; auto &&entry : fs::directory_iterator( PROJECT_NAME ) ) {
			if ( entry.is_directory() ) continue;
			if ( entry.path().extension() != ".gif" ) continue;
			auto name = entry.path().filename().string();
			name.erase( name.length() - 4 );
			try {
				animations[name] = new AnimTexture( _PRODJECT_NAME "/" + name, preloadCounter < kPreloadLimit );
				++preloadCounter;
			} catch ( std::exception &e ) {
				MessageBox( e.what(), "Can't load animation " + name );
			}
		}
//		auto t1 = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );
//		MessageBox( std::to_string( ( t1 - t0 ).count() ) );
		init = true;
	} ).detach();
}

AsiPlugin::~AsiPlugin() {
	if ( did != static_cast<size_t>( -1 ) ) g_class.draw->onDraw -= did;
	if ( mid != static_cast<size_t>( -1 ) ) g_class.events->onMainLoop -= mid;
}

void AsiPlugin::draw() {
	if ( !init ) return;

	setRaster.onBefore += std::tuple{ this, &AsiPlugin::SetRaster };
	setRaster.install( 0, 0, false );

	spriteDraw.onBefore += std::tuple{ this, &AsiPlugin::SpriteDraw };
	spriteDraw.install( 0, 0, false );

	spriteDraw2.onBefore += std::tuple{ this, &AsiPlugin::SpriteDraw2 };
	spriteDraw2.install( 0, 0, false );

	spriteDraw3.onBefore += std::tuple{ this, &AsiPlugin::SpriteDraw3_before };
	spriteDraw3.onAfter += std::tuple{ this, &AsiPlugin::SpriteDraw3_after };
	spriteDraw3.install( 0, 0, false );

	drawWeapon.onBefore += std::tuple{ this, &AsiPlugin::DrawWeapon };
	drawWeapon.install( 0, 0, false );

	mid			= g_class.events->onMainLoop += std::tuple{ this, &AsiPlugin::mainloop };
	gc_lastWork = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );

	auto slotId = did;
	did			= static_cast<size_t>( -1 );
	g_class.draw->onDraw -= slotId;
	init = false;
}

void AsiPlugin::mainloop() {
	auto now = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );

	if ( now - gc_lastWork < 5s ) return;

	auto RamUnload = 10min;
	auto GpuUnload = 5min;
	if ( animations.size() > kPreloadLimit ) {
		RamUnload = 5min;
		GpuUnload = 1min;
	} else if ( animations.size() < kPreloadLimit / 10 ){
		RamUnload = 60min;
		GpuUnload = 15min;
	} else if ( animations.size() < kPreloadLimit / 4 )
		RamUnload = 30min;
	else if ( animations.size() < kPreloadLimit / 2 )
		RamUnload = 20min;

	auto unloaded = 0;
	for ( auto &&[name, anim] : animations ) {
		auto state = anim->getLoadState();
		if ( state == AnimTexture::eLoadState::unload ) {
			++unloaded;
			continue;
		}
		if ( now - anim->getLastUsage() > RamUnload ) {
			anim->setLoadState( AnimTexture::eLoadState::unload );
			++unloaded;
			continue;
		}
		if ( state == AnimTexture::eLoadState::loadInRam ) continue;
		if ( now - anim->getLastUsage() > GpuUnload ) anim->setLoadState( AnimTexture::eLoadState::loadInRam );
	}

	if ( animations.size() - unloaded > kPreloadLimit && now - gc_lastWork > 1min ) {
		std::map<std::chrono::milliseconds, AnimTexture *> unloadList;
		for ( auto &&[name, anim] : animations ) {
			if ( anim->getLoadState() == AnimTexture::eLoadState::unload ) continue;
			auto lastUsage = anim->getLastUsage();
			if ( now - lastUsage < 1s ) continue;
			unloadList[lastUsage] = anim;
		}
		if ( unloadList.size() > kPreloadLimit ) {
			for ( size_t i = 0; i < unloadList.size() - kPreloadLimit; ++i ) {
				unloadList.begin()->second->setLoadState( AnimTexture::eLoadState::unload );
				unloadList.erase( unloadList.begin() );
			}
		}
	}

	gc_lastWork = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );
}

void AsiPlugin::SetRaster( SRHook::CPU &cpu, RwRaster *&raster ) {
	ProcessTexture( (RwTexture *)cpu.EBX, raster );
}

void AsiPlugin::SpriteDraw( SRHook::CPU &cpu ) {
	auto raster = (RwRaster *)cpu.EDX;
	ProcessTexture( (RwTexture *)cpu.EAX, raster );
	cpu.EDX = (size_t)raster;
}

void AsiPlugin::SpriteDraw2( SRHook::CPU &cpu, RwRaster *&raster ) {
	ProcessTexture( (RwTexture *)cpu.EAX, raster );
}

void AsiPlugin::SpriteDraw3_before( SRHook::CPU &cpu ) {
	spriteDraw3Tex = (RwTexture *)cpu.EAX;
}

void AsiPlugin::SpriteDraw3_after( SRHook::CPU &cpu ) {
	auto raster = (RwRaster *)cpu.EAX;
	ProcessTexture( spriteDraw3Tex, raster );
	cpu.EAX = (size_t)raster;
}

void AsiPlugin::DrawWeapon( SRHook::CPU &cpu, RwRaster *&raster ) {
	ProcessTexture( (RwTexture *)cpu.ESI, raster );
}

void AsiPlugin::ProcessTexture( RwTexture *tex, RwRaster *&raster ) {
	if ( !tex ) return;
	auto it = animations.find( tex->name );
	if ( it == animations.end() ) return;
	raster = it->second->animRaster( raster );
}
