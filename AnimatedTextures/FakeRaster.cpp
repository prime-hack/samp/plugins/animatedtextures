#include "FakeRaster.h"

#include <CGame/Types.h>

FakeRaster::FakeRaster() {
	auto pad   = *(DWORD *)0xb4e9e0 - sizeof( RwRaster );
	auto size  = sizeof( RwRaster ) + pad + sizeof( _rwD3D9RasterExt );
	rasterData = new uint8_t[size];
	memset( rasterData, 0, size );
}

FakeRaster::~FakeRaster() {
	delete[] rasterData;
}

RwRaster *FakeRaster::getRaster() const noexcept {
	return (RwRaster *)rasterData;
}

_rwD3D9RasterExt *FakeRaster::getRasterExt() const noexcept {
	return (_rwD3D9RasterExt *)&rasterData[*(DWORD *)0xb4e9e0];
}

void FakeRaster::mimic( RwRaster *raster ) {
	if ( !raster ) return;
	memcpy( getRaster(), raster, sizeof(RwRaster) );
	memcpy( getRasterExt(), raster->rwD3D9RasterExt(), sizeof(_rwD3D9RasterExt) );
}
