#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <SRHook.hpp>
#include <atomic>
#include <map>

struct RwRaster;
struct RwTexture;

class AsiPlugin : public SRDescent {
	static constexpr auto kPreloadLimit = 50; // ~150MB RAM

	SRHook::Hook<struct RwRaster *> setRaster{ 0x7FDFDA, 5 };
	SRHook::Hook<>					spriteDraw{ 0x72837F, 6 };
	SRHook::Hook<struct RwRaster *> spriteDraw2{ 0x728320, 5 };
	SRHook::Hook<>					spriteDraw3{ 0x728570, 8 };
	SRHook::Hook<struct RwRaster *> drawWeapon{ 0x58D872, 5 };

	std::map<std::string, class AnimTexture *> animations;

	std::atomic_bool init = false;
	size_t			 did  = static_cast<size_t>( -1 );
	size_t			 mid  = static_cast<size_t>( -1 );

	RwTexture *spriteDraw3Tex = nullptr;

	std::chrono::milliseconds gc_lastWork{};

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void draw();
	void mainloop();

	void SetRaster( SRHook::CPU &cpu, RwRaster *&raster );
	void SpriteDraw( SRHook::CPU &cpu );
	void SpriteDraw2( SRHook::CPU &cpu, RwRaster *&raster );
	void SpriteDraw3_before( SRHook::CPU &cpu );
	void SpriteDraw3_after( SRHook::CPU &cpu );
	void DrawWeapon( SRHook::CPU &cpu, RwRaster *&raster );

	void ProcessTexture( RwTexture *tex, RwRaster *&raster );
};

#endif // MAIN_H
