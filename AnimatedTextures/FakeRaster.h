#pragma once

#include <cstdint>

struct RwRaster;
struct _rwD3D9RasterExt;

class FakeRaster {
	uint8_t *rasterData = nullptr;
public:
	FakeRaster();
	~FakeRaster();

	RwRaster *		  getRaster() const noexcept;
	_rwD3D9RasterExt *getRasterExt() const noexcept;

	void mimic( RwRaster *raster );
};

