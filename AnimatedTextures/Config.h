#pragma once

#include <string_view>
#include <string>
#include <cstdint>

/**
 * @brief Animation config
 * @details no default ctor, no copy ctor
 */
class Config {
	template<typename T> struct Entry {
		Entry() noexcept { _isInitialized = false; }
		Entry( const T &v ) : _value( v ) {}

		bool isInitialized() const noexcept { return _isInitialized; }

		const T &get() const noexcept { return _value; }
		void	 set( const T &v ) {
			_value		   = v;
			_isInitialized = true;
		}

	private:
		T	 _value;
		bool _isInitialized = true;
	};

public:
	Config()				 = delete;
	Config( const Config & ) = delete;
	Config( std::string_view fname );
	~Config();

	bool isConfigExists() const;

	Entry<bool>		background;
	Entry<bool>		overrideSettings{false}; // always initialized
	Entry<uint8_t>	alpha;
	Entry<uint32_t> paletteNum;

private:
	std::string fname;
};
