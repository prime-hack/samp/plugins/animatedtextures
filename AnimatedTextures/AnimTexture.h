#pragma once

#include "Config.h"
#include "FakeRaster.h"
#include <chrono>

class GifPlayer;

class AnimTexture {
	struct RwPalette {
		int		 pad[256];
		uint32_t paletteNumber = 0;
	};

	Config	   cfg;
	GifPlayer *player = nullptr;
	FakeRaster raster;
	RwPalette *palette	 = nullptr;
	RwRaster * lastMimic = nullptr;

	bool		isEventsInitialized = false;
	std::string gifPath;

public:
	AnimTexture()					   = delete;
	AnimTexture( const AnimTexture & ) = delete;
	AnimTexture( std::string_view name, bool loadNow = true );

	RwRaster *animRaster( RwRaster *originalRaster ) noexcept;

	enum class eLoadState : uint8_t { unload, loadInRam, loadFull };

	eLoadState getLoadState() const;
	void	   setLoadState( eLoadState state );

	std::chrono::milliseconds getLastUsage() const;

protected:
	eLoadState				  loadState = eLoadState::unload;
	std::chrono::milliseconds lastUsage{ 0 };
};
